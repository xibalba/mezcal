<?php

require('../ocelote/vendor/autoload.php');
require('../alpaca/vendor/autoload.php');
require('../mestizo/vendor/autoload.php');
require('vendor/autoload.php');

$config = require('app/spirit/config.php');

xibalba\mezcal\App::getInstance()->setConfig($config);
xibalba\mezcal\App::getInstance()->run();