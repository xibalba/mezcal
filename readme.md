# Mezcal CMS

Yet another PHP CMS to the CMS's sea. So, if out there are a lot, why create a new one?

Well, I want to express some reasons, mostly personal stuff than I want.

* A Wordpress alternative, just because... I want a Wordpress alternative.
* I want a SQLite CMS, for avoid MySQL when you just want a blog.
* I want a CMS that use recent PHP features: namespace, traits, that stuff.
* I want create something Open Source that use my other Open Source projects.
* And I want live the experince of build something like an CMS.

Cheers, @yybalam
