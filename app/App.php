<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal;

use xibalba\mestizo\application\traits\SingletonAware;

use xibalba\mestizo\Application;
use xibalba\mestizo\Router;
use xibalba\mestizo\user\Aware as UserAware;
use xibalba\mestizo\http\session\DbSessionAware;

use xibalba\tuza\DbAware;
use xibalba\tuza\DbConnection;

use xibalba\alpaca\Keeper;
use xibalba\alpaca\exceptions\Alpaca as AlpacaException;

/**
 * Class App
 * @package xibalba\mezcal
 *
 * # Mezcal CMS
 * This is an light CMS, build on top of other Xibalbá projects.
 * This application provide a REST API access for the administration
 * and a traditional web site behavior for content.
 *
 * Read the project's documentation at GitLab page
 *
 * @author E. Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
final class App extends Application {
	use SingletonAware, DbAware, UserAware;

	const VERSION = 'Dev 0.3';

	public function setConfig(array $config) {
		// Some config cheking
		// 1 Views dir
		if(!isset($config['base_views_path'])) $config['base_views_path'] = MZ_APP_DIR . '/views';

		// 2 Template  name
		if(!isset($config['template'])) $config['template'] = 'tequila';

		// 3 User class
		// Change this only if you want to implement your own session system
		if(!isset($config['user_class'])) $config['user_class'] = 'xibalba\mezcal\system\User';

		// 4 Default namespace. This is required by mestizo
		// Change this only if you are changing the namespace
		if(!isset($config['ns'])) $config['ns'] = 'xibalba\mezcal';

		// 5 db
		// If is no setted a db config, then use by default a sqlite file called agave.db
		if(!isset($config['db'])) {
			$config['db'] = [
				'database_type' => 'sqlite',
				'database_file' => MZ_BASE_DIR . '/data/db/agave.db'
			];
		}

		parent::setConfig($config);
		static::setDbConnection(new DbConnection($config['db']));
	}
}
