<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\orm\models;

use xibalba\alpaca\Model;

/**
 * Model class Permission
 * A Permission is a granted right to a role for execute something.
 */
class Permission extends Model {
	protected static $_fields = [
		'resource_id' => ['type' => 'string'],
		'operation_id' => ['type' => 'string'],
		'role_id' => ['type' => 'integer']
	];
}