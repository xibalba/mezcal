<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\orm\models;

use xibalba\mezcal\orm\keepers\Basic as Keeper;

use xibalba\alpaca\Model;

use xibalba\ocelote\ArrayHelper;

/**
 * Model class User
 * This class represent an user of the software.
 * A user (as data) is a set of "credentials", an identification,
 * an unique email and another kind of data that can be considered
 * by custumers or developers.
 */
class User extends Model {

	protected static $_strategy = self::USER_MANUAL_STRATEGY;

	protected static $_fields = [
		'username' => ['type' => 'string'],
		'email' => ['type' => 'string'],
		'password' => ['type' => 'string'],
		'active' => ['type' => 'bool'],
		'person_id' => ['type' => 'string']
	];

	protected static $_idProperty = 'username';

	/**
	 * Retrive an array with user and personal data
	 * @return array
	 */
	public function getDataProfile() {
		// Retrive user tecnical data
		$user = $this->getValues(['username', 'email']);

		// Retrive personal data
		$person = Keeper::fetchById(
			Person::class,
			$this->getValue('person_id'),
			['name', 'surname'],
			Keeper::FETCH_ARRAY
		);

		return ['user' => $user, 'person' => $person];
	}

	public function rehashPassword() {
		$this->setValue('password', hash('sha256', $this->getValue('password')));
	}

	public static function getTableName() : string {
		return 'users';
	}
}
