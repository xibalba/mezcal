<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\orm\models;

use xibalba\alpaca\Model;

/**
 * Model class Person
 * By design the person data is best stored as a separate entity,
 * so if the software grows the person data is stored in a single place and
 * represented by a apopiate class and objects, so the "tecnical user" data,
 * or another "related" data (as costumers or employees) is no mixed with personal data.
 * This design improves the relations between elements (objexts) and software structure.
 *
 * Probably the fields defined by this class are not enough, so feel free to redifine it,
 * but be ensure to reply those changes to the corresponding table.
 */
class Person extends Model {

	protected static $_strategy = self::PHP_UUID_STRATEGY;

	protected static $_fields = [
		// internal id
		'id' => ['type' => 'string'],

		// Basic personal data.
		'name' => ['type' => 'string'],
		'surname' => ['type' => 'string'],
		'gender' => ['type' => 'string']
	];

	public static function getTableName() : string {
		return 'persons';
	}
}
