<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\orm\models;

use xibalba\alpaca\Model;

/**
 * Taxo Model class
 * A Taxo is a taxonomy representation for organize content respect to content.
 * A Taxo element must be used to provide Content of a URI.
 */
class Taxo extends Model {

	protected static $_strategy = self::PHP_UUID_STRATEGY;

	protected static $_fields = [
		'id' => ['type' => 'string'],
		'label' => ['type' => 'string'],
		'description' => ['type' => 'string'],
		'path' => ['type' => 'string']
	];

	/**
	 * @inheritdoc
	 */
	public static function getTableName() : string {
		return 'taxo';
	}
}
