<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\orm\models;

use xibalba\alpaca\Model;

use xibalba\mezcal\orm\keepers\Basic as Keeper;

class Content extends Model {

	protected static $_strategy = self::PHP_UUID_STRATEGY;

	protected static $_fields = [
		'uuid' => ['type' => 'string'],
		'slug' => ['type' => 'string'],

		'title' => ['type' => 'string'],
		'author' => ['type' => 'string'],
		'editor' => ['type' => 'string'],
		'created_at' => ['type' => 'date'],
		'updated_at' => ['type' => 'date'],
		'published_at' => ['type' => 'date'],
		'taxo_id' => ['type' => 'string'],
		'keywords' => ['type' => 'string'],
		'summary' => ['type' => 'string'],
		'ilustration_img' => ['type' => 'string'],
		'status' => ['type' => 'string'],
		'template' => ['type' => 'string'],
		'format' => ['type' => 'string'],
		'body' => ['type' => 'string']
	];

	protected static $_idProperty = 'uuid';

	public function getLink() : string {
		$taxoPath = Keeper::fetchScalarById(Taxo::class, $this->getValue('taxo_id'), 'path');
		return $taxoPath.'/'.$this->getValue('uuid').'/'.$this->getValue('slug');
	}
}
