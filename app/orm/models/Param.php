<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\orm\models;

use xibalba\alpaca\Model;

/**
 * Param Model class
 * This class represent a param element.
 * Params are used for store data by domain when is expensive create a single table
 * for store a bunch of cuasi-static small data and that data is best placed as
 * a database data instead of configuration files.
 */
class Param extends Model {
	protected static $_fields = [
		'id' => ['type' => 'string'],

		'idx' => ['type' => 'string'],
		'val' => ['type' => 'string'],
		'label' => ['type' => 'string'],
		'description' => ['type' => 'string'],
		'domain' => ['type' => 'string'],
		'scalar_type' => ['type' => 'string']
	];

	protected static $_strategy = self::PHP_UUID_STRATEGY;
}