<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\orm\models;

use xibalba\alpaca\Model;

/**
 * Model class Role
 * A Role is a representation of some type of "actor",
 * that is to say a representation of taks and atributions
 * to be assigned to someone.
 */
class Role extends Model {
	protected static $_strategy = self::PHP_UUID_STRATEGY;

	protected static $_fields = [
		'id' => ['type' => 'string'],
		'label' => ['type' => 'string'],
		'description' => ['type' => 'string']
	];
}
