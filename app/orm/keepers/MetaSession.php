<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\orm\keepers;

use xibalba\alpaca\Model as AlpacaModel;

use xibalba\mezcal\App;
use xibalba\mezcal\orm\models\User as UserModel;

use xibalba\ocelote\ArrayHelper;
use xibalba\ocelote\Checker;
use xibalba\ocelote\Inflector;

class MetaSession extends Basic {
	/**
	 * Retrive all roles id for the pased user.
	 *
	 * @return array | bool The fetched array or false on failure.
	 */
	public static function fetchUserRoles($username) {
		return static::getDbConnection()->select('roles_users', 'role_id', ['username' => $username]);
	}

	public static function persist(AlpacaModel ...$models) {
		// var_dump($model->getModifiedFieldsNames()); die();
		// Before persis a model check if password field has been modified.
		foreach($models as $model) {
			if(ArrayHelper::hasValue($model->getModifiedFieldsNames(), 'password')) {
				// Then apply a  rehash to actual value
				$model->rehashPassword();
			}
		}

		// Now call parent
		parent::persist(...$model);
	}
}
