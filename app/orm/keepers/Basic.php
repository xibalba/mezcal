<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\orm\keepers;

use xibalba\mezcal\App;

use xibalba\alpaca\Keeper as BaseKeeper;
use xibalba\tuza\DbConnectionInterface as IDbConnection;

class Basic extends BaseKeeper {
	/**
	 * @inheritdoc
	 */
	public static function getDbConnection() : IDbConnection {
		return App::getDbConnection();
	}
}
