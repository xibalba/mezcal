<?php
/**
 * @copyright	2014 - 2018 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\orm\keepers;

use xibalba\mezcal\App;
use xibalba\mezcal\orm\models\Content as ContentModel;
use xibalba\mezcal\orm\models\Param as ParamModel;

use xibalba\alpaca\Model;

use xibalba\ocelote\Checker;
use xibalba\ocelote\Inflector;

/**
 * Overriden Keeper
 */
class Content extends Basic {

	public static function persist(Model ...$models) {
		foreach($models as $model) {
			if($model->isDirty()) {

				$user = App::getInstance()->getUser();
				$today = new \DateTime();
				$paramStatus = static::fetchById(ParamModel::class, $model->getValue('status'), null, static::FETCH_ARRAY);
	
				if($model->isNew()) {
					$db = static::getDbConnection();
	
					// Content model use by default PHP UUID strategy
					$model->setId(uniqid());
	
					// The autor and editor at create time is the same
					$model->setValues([
						'slug' => Inflector::slug($model->getValue('title')),
						'author' => $user->getSessionData('user_id'),
						'editor' => $user->getSessionData('user_id'),
						'created_at' => $today,
						'updated_at' => $today,
						'format' => 'md'
					]);
	
					if($paramStatus['idx'] == 'published') $model->setValue('published_at', $today);
	
					$values = self::prepareFields($model);
					$db->insert($model::getTableName(), $values);
					static::handleDbError($db->error());
					$model->setLoaded();
				}
				else {
					$model->setValues([
						'editor' => $user->getSessionData('user_id'),
						'updated_at' => new \DateTime(),
					]);
	
					if($paramStatus['idx'] == 'published' && Checker::isEmpty($model->getValue('published_at'))) $model->setValue('published_at', $today);
	
					parent::persist($model);
				}
			}
		}
	}
}
