<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\orm\keepers;

use xibalba\mezcal\orm\models\Person as PersonModel;
use xibalba\mezcal\orm\models\User as UserModel;

use xibalba\ocelote\ArrayHelper;

class User extends Basic {
	public static function fetchRoles(string $username) {
		$db = static::getDbConnection();

		$roles = $db->select(
			'roles_users',
			['roles.label'],
			['roles_users.username' => $username],
			['[><]roles' => ['role_id' => 'id']]
		);

		return $roles;
	}

	public static function fetchWithPersonalData() : array {
		$db = static::getDbConnection();

		$data = $db->select(
			UserModel::getTableName(),
			ArrayHelper::merge(UserModel::getColumnNames(), PersonModel::getColumnNames()),
			['LIMIT' => 20],
			['[><]' . PersonModel::getTableName() => ['person_id' => 'id']]
		);

		$models = [];
		foreach ($data as $row) {
			$user = new UserModel($row);
			$person = new PersonModel($row);

			$uData = $user->getValues();
			$uData['person'] = $person->getValues();

			$models[] = $uData;
		}

		return $models;
	}
}
