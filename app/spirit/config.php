<?php
/**
 * @copyright	2016 - 2018 Xibalba Lab.
 */
define('MZ_APP_DIR', realpath(__DIR__ . '/../'));
define('MZ_BASE_DIR', realpath(__DIR__ . '/../../'));
define('MZ_DEBUG', true);

return [
	'title' => 'Mezcal CMS',
	'name' => 'Mezcal CMS',

	'ns' => 'xibalba\mezcal',

	'routes' => [
		'/' => [
			'controller' => 'Distpatcher',
			'action' => 'index'
		],

		'/api/rest/session' => [
			'namespace' => 'restapi',
			'controller' => 'Session',
			'action' => '',

			'methods' => ['POST', 'DELETE']
		],

		'/api/rest/session/{action}' => [
			'namespace' => 'restapi',
			'controller' => 'Session',
			'action' => ['key' => 'action', 'regex' => '(\w+)'],

			'methods' => ['GET', 'PUT']
		],

		'/api/rest/config/{controller}' => [
			'namespace' => 'restapi\config',
			'controller' => ['key' => 'controller', 'regex' => '(\w+)'],
			'action' => '',

			'methods' => ['GET', 'POST']
		],

		'/api/rest/config/{controller}/{id}' => [
			'namespace' => 'restapi\config',
			'controller' => ['key' => 'controller', 'regex' => '(\w+)'],
			'action' => '',

			'params' => [
				['key' => 'id', 'regex' => '(\w+)']
			],

			'methods' => ['PUT']
		],

		'/api/rest/{controller}' => [
			'namespace' => 'restapi',
			'controller' => ['key' => 'controller', 'regex' => '(\w+)'],
			'action' => '',

			'methods' => ['GET', 'POST']
		],

		'/api/rest/{controller}/{action}' => [
			'namespace' => 'restapi',
			'controller' => ['key' => 'controller', 'regex' => '(\w+)'],
			'action' => ['key' => 'action', 'regex' => '(\w+)'],

			'methods' => ['GET']
		],

		'/api/rest/{controller}/{id}' => [
			'namespace' => 'restapi',
			'controller' => ['key' => 'controller', 'regex' => '(\w+)'],
			'action' => '',

			'params' => [
				['key' => 'id', 'regex' => '(\w+)']
			],

			'methods' => ['GET', 'PUT']
		],

		'/author/{username}' => [
			'controller' => 'Distpatcher',
			'action' => 'author',

			'params' => [
				['key' => 'username',  'regex' => '(\w+)']
			],

			'methods' => ['GET']
		],

		'/flat/{path}/{slug}' => [
			'controller' => 'flat\Distpatcher',
			'action' => 'page',

			'params' => [
				['key' => 'path',  'regex' => '(^\w+)(\/[a-z0-9-]+)*']
				//['key' => 'slug',  'regex' => '^[a-z0-9-]+']
			]
		],

		'/{taxo}/{id}/{slug}' => [
			'controller' => 'Distpatcher',
			'action' => 'page',

			'params' => [
				['key' => 'taxo',  'regex' => '(\w+)'],
				['key' => 'id',  'regex' => '(\w+)'],
				['key' => 'slug',  'regex' => '(\w+)']
			],

			'methods' => ['GET']
		],
	],

	'base_domain' => 'http://mezcal.lab/',

	'template' =>  'tate',
	'base_views_path' => MZ_BASE_DIR . '/templates',

	'user_class' => 'xibalba\mezcal\spirit\User',
	/*'session' => [
		'handler_class' => 'Db'
	]*/
];
