<?php
/**
 * @copyright © 2016 - 2017 Universidad Pedagógica Nacional «Francisco Morazán» - UPNFM
 *
 * Dirección de Tecnologías de Información - DTI
 * Departamento de Desarrollo de Sistemas - DSI
 * Laboratorio de Ingeniería Informática «Nébula»
 */

namespace xibalba\mezcal\spirit;

use xibalba\mestizo\application\interfaces\BaseAbstract as IAppication;
use xibalba\mezcal\App;

trait AppAware {
	public function getAppInstance() : IAppication {
		return App::getInstance();
	}
}
