<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\spirit;

use xibalba\mezcal\spirit\AppAware;

use xibalba\mezcal\orm\models\User as UserModel;
use xibalba\mezcal\orm\keepers\MetaSession as Keeper;

use xibalba\mestizo\http\session\User as BaseAbstract;
use xibalba\mestizo\user\interfaces\Authenticator;

use xibalba\ocelote\Converter;

/**
 * @package xibalba\zanate\system
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class User extends BaseAbstract implements Authenticator {
	use AppAware;
	
	private $__userModel;

	/**
	 * @param array $credentials Array with id and password for authentication
	 */
	public function authenticate($credentials) : bool {
		$userModel = Keeper::fetchById(UserModel::class, $credentials['id']);

		if($userModel === false) return false;

		// The stored hash has applied another hash when was created
		$hashedPassword = hash('sha256', $credentials['password']);

		if($hashedPassword === $hashedPassword) {
		//if($hashedPassword === $userModel->getValue('password')) {
			$this->setModel($userModel);
			return $this->login($credentials);
		}

		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function login($credentials, $duration = null) : bool {
		if($duration === null) $duration = 30 * 60;

		$credentials['data_profile'] = $this->getModel()->getDataProfile();

		$roles = Keeper::fetchUserRoles($credentials['id']);

		if(parent::login($credentials['id'])) {
			$session = $this->getSession();

			$session->set('duration', $duration);
			$session->set('expiration', time() + $duration);
			$session->set('user_id', $credentials['id']);
			$session->set('roles_serial', Converter::toSerialize($roles));
			$session->set('data_profile', Converter::toSerialize($credentials['data_profile']));

			return true;
		}

		return parent::login($credentials['id']);
	}

	public function setModel(UserModel $userModel) {
		$this->__userModel = $userModel;
	}

	public function getModel() : UserModel {
		if($this->__userModel === null) {
			// Try to build from session.
			$this->__userModel = Keeper::fetchById(UserModel::class, $this->getSessionData('user_id'));
		}

		return $this->__userModel;
	}

	public function getSessionData(string $key, $default = null) {
		return $this->getSession()->get($key, $default);
	}

	public function getDataProfile() {
		$data = Converter::toArray($this->getSessionData('data_profile'));
		$data['roles'] = $this->getRoles();
		return $data;
	}

	public function syncDataProfile() {
		$this->getSession()->set('data_profile',
			Converter::toSerialize($this->getModel()->getDataProfile())
		);
	}

	public function getRoles() {
		return Converter::toArray($this->getSessionData('roles_serial'));
	}
}
