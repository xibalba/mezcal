<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\controllers\filters;

use xibalba\mezcal\App;

use xibalba\mestizo\controller\interfaces\ActionFilter;

use xibalba\mestizo\http\Http;
use xibalba\mestizo\http\Exception as HttpException;

class Authorization implements ActionFilter {
	public function beforeAction(string $actionId) : bool {
		// Check for user session
		if(App::getInstance()->getUser()->isLogged()) return true;
		throw new HttpException('User has no session. [401-1]', Http::UNAUTHORIZED);
	}
}
