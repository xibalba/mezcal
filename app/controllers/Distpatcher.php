<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\controllers;

use Psr\Http\Message\ResponseInterface as IResponse;

use xibalba\mezcal\App;
use xibalba\mestizo\controller\WebPage;

use xibalba\mestizo\http\Exception as HttpException;
use xibalba\mestizo\http\Http;

use xibalba\mezcal\orm\models\Param as ParamModel;
use xibalba\mezcal\orm\models\Content as ContentModel;
use xibalba\mezcal\orm\keepers\Content as ContentKeeper;

use xibalba\mestizo\application\interfaces\BaseAbstract as IAppication;

/**
 * This controller is the main content distpatcher.
 *
 * @package xibalba\mezcal\controllers
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Distpatcher extends WebPage {
	public function __construct(Request $request = null) {
		parent::__construct($request);

		// Set views dir name from template config
		$this->setViewDirname(App::getInstance()->getConfig('template'));
	}

	public function index() : IResponse {
		$paginNo = $this->getRequest()->getQueryParam('page', 1);

		// limit at home a scalar value
		$limit = 12; // @todo set as param value

		$statusPublished = ContentKeeper::fetchFirstBy(
			ParamModel::class, [
				'AND' => ['idx' => 'published', 'domain' => 'content_status']
			], null, ContentKeeper::FETCH_ARRAY
		);

		$conditions = [
			'LIMIT' => $limit,
			'status' => $statusPublished['id'],
			'ORDER' => ['published_at' => 'DESC']
		];

		$contents = ContentKeeper::fetchBy(
			ContentModel::class,
			null,
			$conditions
		);

		return $this->render('home', [
			'contents' => $contents
		]);
	}

	public function page() : IResponse {
		$cid = $this->getRequest()->getQueryParam('id');

		$content = ContentKeeper::fetchById(ContentModel::class, $cid);
		if(!$content) throw new HttpException('Content not found', Http::NOT_FOUND);

		return $this->render('page', [
			'content' => $content
		]);
	}

	public function author() : IResponse {
		$username = $this->getRequest()->getQueryParam('username');

		$statusPublished = ContentKeeper::fetchFirstBy(
			ParamModel::class, [
				'AND' => ['idx' => 'published', 'domain' => 'content_status']
			], null, ContentKeeper::FETCH_ARRAY
		);

		$conditions = [
			'AND' => [
				'status' => $statusPublished['id'],
				'author' => $username
			],
			'LIMIT' => 20,
			'ORDER' => ['published_at' => 'DESC']
		];

		$contents = ContentKeeper::fetchBy(ContentModel::class, null, $conditions);

		// if(!$contents) throw new HttpException('Content not found', Http::NOT_FOUND);

		return $this->render('author', [
			'contents' => $contents
		]);
	}

	public function getAppInstance() : IAppication {
		return App::getInstance();
	}
}
