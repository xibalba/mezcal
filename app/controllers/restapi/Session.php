<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\controllers\restapi;

use Psr\Http\Message\ResponseInterface as IResponse;

use xibalba\mezcal\App;

use xibalba\mezcal\controllers\interfaces\Session as ISession;

use xibalba\mestizo\controller\Rest;
use xibalba\mestizo\http\Exception as HttpException;
use xibalba\mestizo\http\Http;

use xibalba\mezcal\orm\keepers\MetaSession as Keeper;
use xibalba\mezcal\orm\models\User;
use xibalba\mezcal\orm\models\Person;

use xibalba\ocelote\ArrayHelper;
use xibalba\ocelote\Converter;
use xibalba\ocelote\Checker;

/**
 * class Session
 *
 * This class provide an API to manipulate all related data about user itself
 * also login and logout funtionaliny over standar plain HTTP.
 *
 * @author E. Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Session extends BaseAbstract implements ISession {
	/**
	 * @inheritdoc
	 */
	public function post() : IResponse {
		$user = App::getInstance()->getUser();
		$serverParams = $this->getRequest()->getServerParams();

		$credentials = [
			'id'		=> $serverParams['PHP_AUTH_USER'],
			'password'  => $serverParams['PHP_AUTH_PW']
		];

		if($user->authenticate($credentials)) {
			return $this->respondCreated([
				'success' => true,
				'logged' => $user->isLogged()
			]);
		}

		throw new HttpException('Failed Authentication.', Http::UNAUTHORIZED);
	}

	/**
	 * @inheritdoc
	 */
	public function delete() : IResponse {
		App::getInstance()->getUser()->logout();
		return $this->respondNoContent();
	}

	/**
	 * @inheritdoc
	 */
	public function getProfile() :IResponse {
		$user = App::getInstance()->getUser();
		$profile = $user->getDataProfile();

		return $this->respond([
			'success' => true,
			'profile' => $profile
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function putProfile() : IResponse {
		$user = App::getInstance()->getUser();
		$userData = Converter::toArray($this->getRequest()->getBodyParam('user'));
		$personalData = Converter::toArray($this->getRequest()->getBodyParam('person'));

		if(!Checker::isEmpty($userData) || !Checker::isEmpty($personalData)) {
			// Ensure delete password if sended
			if(ArrayHelper::keyExists('password', $userData)) ArrayHelper::remove('password', $userData);

			try {
				if(!Checker::isEmpty($userData)) {
					$userModel = $user->getModel();
					$userModel->setValues($userData);
					Keeper::persist($userModel);

					// @todo No estoy seguro de que sea necesaria este llamada
					$user->setModel($userModel);
				}

				if(!Checker::isEmpty($personalData)) {
					Keeper::updateById(Person::class, $userModel->getValue('person_id'), $personalData);
				}

				// Esta si es necesaria.
				$user->syncDataProfile();

				return $this->respond([
					'success' => true,
					'profile' => $user->getDataProfile()
				]);
			}
			catch(DbException $e) {
				$msg = 'Internal server error.';
				if(MZ_DEBUG) $msg .= ' ' . $e->getMessage() . ' ' . Keeper::getDbConnection()->getLastQuery();
				throw new HttpException($msg, Http::INTERNAL_SERVER_ERROR);
			}
		}

		throw new HttpException('No data has been recived.', Http::NO_CONTENT);
	}

	/**
	 * @inheritdoc
	 */
	public function putPassword() : IResponse {
		// @todo Must require actual password before change to new one.

		// The incoming password must be a SHA256 hash
		// composed by the `username` concatenaded with the plain text password
		$newPassword = $this->getRequest()->getBodyParam('newpassword');

		if(!Checker::isEmpty($newPassword)) {
			$user = App::getInstance()->getUser();
			$userModel = $user->getModel();
			$userModel->setValue('password', $newPassword);

			try {
				Keeper::persist($userModel);
				return $this->respond(['success' => true]);
			}
			catch(DbException $e) {
				$msg = 'Internal server error.';
				if(MZ_DEBUG) $msg .= ' ' . $e->getMessage();
				throw new HttpException($msg, Http::INTERNAL_SERVER_ERROR);
			}
		}

		throw new HttpException('No data has been recived.', Http::NO_CONTENT);
	}

	/**
	 * @inheritdoc
	 */
	public function getPermissions() : IResponse {
		// Hardcode @todo a more dinamic way
		// There is 3 "hard roles": admin, editor and author

		$roles = App::getInstance()->getUser()->getRoles();
		$permissions = [];

		foreach ($roles as $role) {
			if($role == 'admin') {
				// An admin have permissions to content, taxonomy and config
				$permissions[] = 'content';
				$permissions[] = 'taxo';
				$permissions[] = 'config';
			}

			if($role == 'editor') {
				// An editor have permissions to content and taxonomy
				$permissions[] = 'content';
				$permissions[] = 'taxo';
			}

			if($role == 'author') {
				// An author have permissions just to content
				$permissions[] = 'content';
			}
		}

		// Remove possible duplicates values
		$permissions = array_unique($permissions);

		return $this->respond([
			'success' => true,
			'permissions' => $permissions
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function beforeAction(string $id) : bool {
		if($this->getRequest()->getMethod() === 'POST' && $id === '') {
			if (!App::getInstance()->getUser()->isLogged()) return true;
			throw new HttpException('User is logged.', Http::NO_CONTENT);
		}
		else return parent::beforeAction($id);
	}
}
