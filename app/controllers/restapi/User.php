<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\controllers\restapi;

use Psr\Http\Message\ResponseInterface as IResponse;

use xibalba\mezcal\orm\models\Person as PersonModel;
use xibalba\mezcal\orm\models\User as UserModel;
use xibalba\mezcal\orm\keepers\Basic as Keeper;
use xibalba\mezcal\orm\keepers\User as UserKeeper;

use xibalba\mezcal\orm\keepers\MetaSession as MetaSessionKeeper;

use xibalba\ocelote\Converter;
use xibalba\ocelote\Checker;

class User extends BaseAbstract {
	public function get() : IResponse {
		$userId = $this->getRequest()->getQueryParam('id');

		if(!Checker::isEmpty($userId)) {
			$user = Keeper::fetchById(UserModel::class, $userId);
			$person = Keeper::fetchById(PersonModel::class, $user->getValue('person_id'));

			$userData = $user->getValues();
			$userData['person'] = $person->getValues();

			return $this->respond([
				'success' => true,
				'user' => $userData
			]);
		}

		$users = UserKeeper::fetchWithPersonalData();

		return $this->respond([
			'success' => true,
			'users' => $users
		]);
	}

	public function getRoles() : IResponse {
		$username = $this->getRequest()->getQueryParam('username');
		$roles = UserKeeper::fetchRoles($username);

		return $this->respond([
			'success' => true,
			'roles' => $roles
		]);
	}

	public function post() : IResponse {
		$userData = Converter::toArray($this->getRequest()->getBodyParam('user'));
		$personData = Converter::toArray($this->getRequest()->getBodyParam('person'));

		if(!Checker::isEmpty($userData)) {
			$user = new UserModel($userData);
			$person = new PersonModel($personData);

			try {
				Keeper::persist($person);

				$user->setValue('person_id', $person->getId());
				$user->setValue('active', true);

				MetaSessionKeeper::persist($user);

				$toReturn = $user->getValues();
				$toReturn['person'] = $person->getValues();

				return $this->respondCreated([
					'success' => true,
					'user' => $toReturn
				]);
			}
			catch(DbException $e) {
				$msg = 'Internal server error.';
				if(MZ_DEBUG) $msg .= ' ' . $e->getMessage();
				throw new HttpException($msg, Http::INTERNAL_SERVER_ERROR);
			}
		}

		throw new HttpException('No data has been recived.', Http::NO_CONTENT);
	}

	public function put() : Response {
		$id = $this->getRequest()->getQueryParam('id');
		$userData = Converter::toArray($this->getRequest()->getBodyParam('user'));
		$personData = Converter::toArray($this->getRequest()->getBodyParam('person'));

		if(!Checker::isEmpty($userData)) {
			$user = Keeper::fetchById(UserModel::class, $id);
			if($user === false) throw new HttpException('User was not found.', Http::NO_CONTENT);
			$person = Keeper::fetchById(PersonModel::class, $user->getValue('person_id'));

			$user->setValues($userData);
			$person->setValues($personData);

			try {
				MetaSessionKeeper::persist($user);
				Keeper::persist($person);

				$toReturn = $user->getValues();
				$toReturn['person'] = $person->getValues();

				return $this->respondCreated([
					'success' => true,
					'user' => $toReturn,
					'id' => $id
				]);
			}
			catch(DbException $e) {
				$msg = 'Internal server error.';
				if(MZ_DEBUG) $msg .= ' ' . $e->getMessage();
				throw new HttpException($msg, Http::INTERNAL_SERVER_ERROR);
			}
		}

		throw new HttpException('No data has been recived.', Http::NO_CONTENT);
	}
}
