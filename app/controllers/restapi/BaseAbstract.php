<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\controllers\restapi;

use xibalba\mezcal\spirit\AppAware;

use xibalba\mestizo\application\interfaces\BaseAbstract as IAppication;

use xibalba\mezcal\controllers\filters\Authorization;

use xibalba\mestizo\controller\Rest;
use xibalba\mestizo\http\responder\Json as JsonResponder;

/**
 * @inheritdoc
 */
class BaseAbstract extends Rest {
	use AppAware, JsonResponder;

	protected function init() {
		$this->addActionFilter(new Authorization());
	}

	/**
	 * This method expect a models array to extract data by call `getValues()` method for
	 * each model and push it into an standar array.
	 * By this way the result aray have data with any process (ilter or formats, by example)
	 * that execute the `getValue()` method of the model.
	 */
	public function extractModelValues(array $data) {
		$r = [];
		foreach ($data as $model) $r[] = $model->getValues();
		return $r;
	}
}
