<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\controllers\restapi;

use Psr\Http\Message\ResponseInterface as IResponse;

use xibalba\mezcal\orm\models\Taxo as TaxoModel;
use xibalba\mezcal\orm\keepers\Basic as Keeper;

use xibalba\ocelote\Converter;
use xibalba\ocelote\Checker;

class Taxo extends BaseAbstract {
	public function get() : IResponse {
		$id = $this->getRequest()->getQueryParam('id');
		$includeAllItemsOption = $this->getRequest()->getQueryParam('include_all_items_option', false);
		$taxo = [];

		if(!Checker::isEmpty($id)) $taxo = Keeper::fetchById(TaxoModel::class, $id, null, Keeper::FETCH_ARRAY);
		else $taxo = Keeper::fetchAll(TaxoModel::class, null, Keeper::FETCH_ARRAY);

		return $this->respond(['taxo' => $taxo]);
	}

	public function post() : IResponse {
		$data = Converter::toArray($this->getRequest()->getBodyParam('taxo'));

		if(!Checker::isEmpty($data)) {
			$taxo = new TaxoModel($data);

			try {
				Keeper::persist($taxo);

				return $this->respondCreated([
					'success' => true,
					'taxo' => $taxo->getValues()
				]);
			}
			catch(DbException $e) {
				throw new HttpException('Internal server error.', Http::INTERNAL_SERVER_ERROR);
			}
		}

		return $this->respond([
			'success' => false,
			'msg' => 'No data has been recived.'
		], Http::NO_CONTENT);
	}

	public function put() : IResponse {
		$id = $this->getRequest()->getQueryParam('id');

		if(!Checker::isEmpty($id)) {
			$data = Converter::toArray($this->getRequest()->getBodyParam('taxo'));

			try {
				Keeper::updateById(TaxoModel::class, $id, $data);
				return $this->respond(['success' => true]);
			}
			catch(DbException $e) {
				throw new HttpException(
					'Internal server error. '
					//.$e->getMessage()
					//.' SQL: '.Keeper::getDbConnection()->last_query()
					, Http::INTERNAL_SERVER_ERROR);
			}
		}
		return $this->respond(['success' => false], Http::NOT_FOUND);
	}
}
