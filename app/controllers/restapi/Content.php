<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\controllers\restapi;

use xibalba\mezcal\controllers\interfaces\Content as IContent;

use xibalba\mezcal\App;
use xibalba\mezcal\orm\models\Content as ContentModel;
use xibalba\mezcal\orm\keepers\Content as ContentKeeper;

use Psr\Http\Message\ResponseInterface as IResponse;

use xibalba\mestizo\http\Exception as HttpException;
use xibalba\mestizo\http\Http;

use xibalba\tuza\DbException;

use xibalba\ocelote\ArrayHelper;
use xibalba\ocelote\Checker;
use xibalba\ocelote\Converter;

/**
 * @inheritdoc
 */
class Content extends BaseAbstract implements IContent {
	// @var array Operations that are non part of public API but must be assigned as one to roles.
	private static $__flagOperations = [
		// `publish` allow to a user to set the content state to «published»
		'publish',
		// `editNonOwn` allow to edit a content when is not the author
		'editNonOwn'
	];

	public function get() : IResponse {
		$contentId = $this->getRequest()->getQueryParam('id');

		if(!Checker::isEmpty($contentId)) {
			$content = ContentKeeper::fetchById(ContentModel::class, $contentId);

			return $this->respond([
				'success' => true,
				'content' => $content->getValues()
			]);
		}

		$taxoId = $this->getRequest()->getQueryParam('taxo_id');

		if(!Checker::isEmpty($taxoId)) {
			$content = ContentKeeper::fetchBy(ContentModel::class, null, ['taxo_id' => $taxoId]);

			return $this->respond([
				'success' => true,
				'content' => $this->extractModelValues($content)
			]);
		}

		$criteria = Converter::toArray($this->getRequest()->getQueryParam('criteria'));
		$user = App::getInstance()->getUser();

		// By default retrive just for actual user
		$conditions = [
			'author' => $user->getSessionData('user_id'),
			'editor' => $user->getSessionData('user_id'),
		];

		$roles = $user->getRoles();
		// @todo An editor must be able to get from all authors but just its own editions
		if(array_search('editor', $roles) !== false || array_search('admin', $roles) !== false) {
			$conditions = [];
		}

		// @todo add support for date ranges, status and other fields
		if(!Checker::isEmpty($criteria)) {
			if(ArrayHelper::keyExists('taxo_id', $criteria)) $conditions['taxo_id'] = $criteria['taxo_id'];

			// If there is author or editor as criteria value then override actual conditions
			if(ArrayHelper::keyExists('author', $criteria)) $conditions['author'] = $criteria['author'];
			if(ArrayHelper::keyExists('editor', $criteria)) $conditions['editor'] = $criteria['editor'];
		}

		if(count($conditions) > 1) $conditions = ['AND' => $conditions, 'LIMIT' => 20];
		else $conditions['LIMIT'] = 20;

		// @todo take from some configuration value
		$contents = ContentKeeper::fetchBy(
			ContentModel::class,
			null,
			$conditions
		);

		return $this->respond([
			'success' => true,
			'content' => $this->extractModelValues($contents)
		]);
	}

	public function post() : IResponse {
		$data = Converter::toArray($this->getRequest()->getBodyParam('content'));

		if(!Checker::isEmpty($data)) {
			$content = new ContentModel($data);

			try {
				ContentKeeper::persist($content);


				return $this->respondCreated([
					'success' => true,
					'content' => $content->getValues()
				]);
			}
			catch(DbException $e) {
				$msg = 'Internal server error.';
				if(MZ_DEBUG) $msg .= ' ' . $e->getMessage();
				throw new HttpException($msg, Http::INTERNAL_SERVER_ERROR);
			}
		}

		throw new HttpException('No data has been recived.', Http::NO_CONTENT);
	}

	public function put() : IResponse {
		$id = $this->getRequest()->getQueryParam('id');
		$data = Converter::toArray($this->getRequest()->getBodyParam('content'));

		if(!Checker::isEmpty($data)) {
			$content = ContentKeeper::fetchById(ContentModel::class, $id);
			if($content === false) throw new HttpException('Content was not found.', Http::NO_CONTENT);
			$content->setValues($data);

			try {
				ContentKeeper::persist($content);

				return $this->respond([
					'success' => true,
					'content' => $content->getValues()
				]);
			}
			catch(DbException $e) {
				$msg = 'Internal server error.';
				if(MZ_DEBUG) $msg .= ' ' . $e->getMessage();
				throw new HttpException($msg, Http::INTERNAL_SERVER_ERROR);
			}
		}

		throw new HttpException('No data has been recived.', Http::NO_CONTENT);
	}

	/**
	 * @inheritdoc
	 */
	public function getFlagOperations() : array {
		return static::$__flagOperations;
	}
}
