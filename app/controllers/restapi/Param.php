<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\controllers\restapi;

use Psr\Http\Message\ResponseInterface as IResponse;

use xibalba\mezcal\orm\keepers\Basic as Keeper;
use xibalba\mezcal\orm\models\Param as ParamModel;

class Param extends BaseAbstract {
	public function getContentStatus() : IResponse {
		$status = Keeper::fetchBy(ParamModel::class, null, ['domain' => 'content_status'], Keeper::FETCH_ARRAY);
		
		return $this->respond([
			'success' => true,
			'status' => $status
		]);
	}
}