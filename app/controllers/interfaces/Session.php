<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\controllers\interfaces;

use Psr\Http\Message\ResponseInterface as IResponse;
use http\Client\Response;

interface Session extends Post, Delete {
	/**
	 * Retrive profile information.
	 */
	public function getProfile() : IResponse;

	/**
	 * Retrive allowed resources for actual session user.
	 */
	public function getPermissions() : IResponse;

	/**
	 * Update the general profile data.
	 *
	 * @return IResponse With some status code for sucess or failure
	 */
	public function putProfile() : IResponse;

	/**
	 * Update user password.
	 *
	 * The password data is not actualizable via `putProfile()` method since
	 * that require some extra validation. So, the functionality to change
	 * the user password reside on this methods.
	 *
	 * @return Response [description]
	 */
	public function putPassword() : IResponse;
}
