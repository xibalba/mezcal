<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\controllers\interfaces;

use Psr\Http\Message\ResponseInterface as IResponse;

interface Get {
	/**
	 * Retrive some data
	 */
	public function get() : IResponse;
}
