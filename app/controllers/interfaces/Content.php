<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mezcal
 */

namespace xibalba\mezcal\controllers\interfaces;

use Psr\Http\Message\ResponseInterface as IResponse;

/**
 * Content
 *
 * A Content is the central element of a CMS, a composed document with text, images, and other media.
 * The CMS goal is the handling of relation between content and data useful to users,
 * such author, editor, dates, status, etc.
 *
 * For manipulate the content data must be applied some restrictions to roles that
 * can archive, so some user in function by hes/her roles can perfome some operation sush
 * edit non own content, set some specific status or even some hard operations like "create" or
 * "update" content.
 *
 * This interface define the public API to be avaible for REST request, also the particular
 * behaivors that must be implemented as a valid Content handler.
 *
 * @author E. Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
interface Content {
	/**
	 * Retrive
	 *
	 * An allowed user can retrive content. The user can be limited by some criteria,
	 * like role and particular flags. By default an user must retrive only own content,
	 * but can be set flag operation to allow retrive non own content.
	 *
	 * @return IResponse
	 */
	public function get() : IResponse;

	/**
	 * Create
	 *
	 * Create new content. By default any logged user with a role allowed to the
	 * content resource (the controller that implements this interface) can create content.
	 * For prevent to other registred user of create content just do not assign it roles
	 * that allow this resource.
	 *
	 * @return IResponse
	 */
	public function post() : IResponse;

	/**
	 * Update
	 *
	 * Update content data. By default any logged user with a role alloed to the
	 * content resource (the controller that implements this interface) can update own content.
	 * A role can be allowed to update non own content.
	 *
	 * @return IResponse
	 */
	public function put() : IResponse;

	/**
	 * Retrive flag operations.
	 *
	 * Some operations to archive over content are not requierd to be implemented
	 * as a controller method, instead is a flag that is used by the actual methods
	 * to be evaluated and permorfm some specific logic.
	 *
	 * @return array
	 */
	public function getFlagOperations(): array;
}
