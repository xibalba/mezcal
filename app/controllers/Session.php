<?php
/**
 * @copyright 	2016 Xibalba Lab.
 * @license     http://opensource.org/licenses/bsd-license.php
 * @link 		https://bitbucket.org/xibalba/zanate
 */

namespace xibalba\mezcal\controllers;

use xibalba\mezcal\App;

use xibalba\mezcal\controllers\api\BaseAbstract;

use xibalba\mestizo\http\Exception as HttpException;
use xibalba\mestizo\http\Http;

class Session extends BaseAbstract {
	public function post() {
		$user = App::getInstance()->getUser();
		$credentials = [
			'id' => $this->getRequest()->server->get('PHP_AUTH_USER'),
			'pass' => $this->getRequest()->server->get('PHP_AUTH_PW')
		];

		if($user->authenticate($credentials)) {
			$data = [
				'id' => 1,
				'username' => 'yrodas',
				'email' => 'yybalam@gmx.com',
				'name' => 'Yeshua Rodas'
			];

			return $this->respond([
				'success' => true,
				'profile' => $data
				//'profile' => $user->getModel()->getValues([])
			]);
		}

		throw new HttpException('Invalid user credentials. Please try again.', Http::UNAUTHORIZED);
	}

	public function getProfile() {
		$user = App::getInstance()->getUser();
		return $this->respond(['sucess' => true, 'profile' => $user->getDataProfile()]);
	}
}
